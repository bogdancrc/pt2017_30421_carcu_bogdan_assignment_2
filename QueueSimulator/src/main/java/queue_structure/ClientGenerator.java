package queue_structure;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
/*
 * Class that creates, based on the future user input,
 * a list of clients holding randomized attributes,
 * with respect to the specified ranges.
 */
public class ClientGenerator {

	private List<Client> list;
	
	ClientGenerator(int minArrivalTime, int maxArrivalTime, int minServiceTime, int maxServiceTime, int simulation) {
		
		list = new ArrayList<Client>();
		Random rand;
		int i = 0;
		Client c;
		
		while(i < simulation) {
			
			rand = new Random();
			int arrivalTime = rand.nextInt(maxArrivalTime - minArrivalTime + 1) + minArrivalTime;
			int serviceTime = rand.nextInt(maxServiceTime - minServiceTime + 1) + minServiceTime;
		
			if(serviceTime < 0)
				break;
			
			if(i + arrivalTime >= simulation)
				break;
			
			c = new Client(i + arrivalTime, serviceTime);
			list.add(c);
			i += arrivalTime;
			
		}
	}
		
	public List<Client> getList() {
		
		return list;
		
	}
	
}
