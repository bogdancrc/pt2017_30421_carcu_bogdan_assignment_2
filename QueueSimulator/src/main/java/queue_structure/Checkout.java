package queue_structure;

import java.util.List;

/*
 * Main class for building, handling and simulating
 * the queues' structure and evolution.
 * Also, provides methods for a set of statistics 
 * to be displayed
 */
public class Checkout {

	//needed info
	private List<Client> clientList;
	private ClientQueue[] queues;
	private int simulation;  //for our simulation time, we suppose: 1m = 1s = 1500ms
	private boolean isActive;
	
	//expected info
	private int averageWaitingTime;
	private int emptyQueueTime;
	private int totalServiceTime;
	private int peak;

	/*
	 * Checkout constructor will take into account every detail
	 * which will be specified by the user in the UI section
	 * later on
	 */
	public Checkout(int simulation, int numberOfQueues, int minArrivalTime, int maxArrivalTime, 
			int minServiceTime, int maxServiceTime) throws Exception {
		
		if(simulation < 0 || numberOfQueues < 1 || minArrivalTime < 0 || maxArrivalTime < 0 ||
			minServiceTime < 0 || maxServiceTime < 0)
		throw new Exception();
		
		clientList = (new ClientGenerator(minArrivalTime, maxArrivalTime, minServiceTime, maxServiceTime, simulation)).getList();
		this.simulation = simulation;
		queues = new ClientQueue[numberOfQueues];
		
		for(int i=0; i< numberOfQueues; ++i)
			queues[i] = new ClientQueue();
		
		for(Client c: clientList) 
			totalServiceTime += c.getServiceTime();
		
		isActive = true;
	}
	
	/*
	 * returns the index of the smallest queue
	 * in size
	 */
	private int getSmallestIndex(ClientQueue[] q) {
		
		int min = 0;
		for(int i=0; i<q.length; ++i)
			if(q[min].getQueueSize() > q[i].getQueueSize())
				min = i;
		
		return min;
	}
	/*
	 * checks if all queues are empty simultaneously
	 */
	private boolean areQueuesEmpty(ClientQueue[] q) {
		
		for(int i=0; i<q.length; ++i)
			if(q[i].getQueueSize() != 0) 
				return false;
		
		return true;
	}
	
	private int getCurrentNumberOfClients() {
		
		int sum = 0;
		
		for(ClientQueue q: queues)
			sum += q.getQueueSize();
		
		return sum;
		
	}
	
	/*
	 * -> method to distribute clients into queues;
	 * -> returns a Thread object with the corresponding
	 *    behavior.
	 */
	public Thread enterQueues() {
		
		Thread enter = new Thread(new Runnable() {
		int maxNumber = 0;

			public void run() {
				System.out.println("~~~~~ Starting Simulation ~~~~~\n");
				for(int i=0; i<=simulation; ++i) {
					if(!isActive) break;
					if(maxNumber < getCurrentNumberOfClients()) {
						maxNumber = getCurrentNumberOfClients();
						peak = i;
					}
					try {
						Thread.sleep(1500);
					} catch (InterruptedException e) {
					}
	
					if(!clientList.isEmpty()) {
						if(clientList.get(0).getArrivalTime() == i) {
							ClientQueue smallestQueue = queues[getSmallestIndex(queues)];
							smallestQueue.enqueue(clientList.get(0));
							clientList.remove(0);
						}		
					} else break;
				}
			}
		});
		return enter;
	}
	
	/*
	 * -> method to extract clients from queues
	 *    when their service time runs out;
	 * -> returns a Thread object with the corresponding
	 *    behavior.
	 */
	public Thread exitQueues() {
		
	Thread exit = new Thread(new Runnable() {
		
		public void run() {			
			for(int i=0; i<simulation; ++i) {
				if(!isActive) return;
				try {
					Thread.sleep(1500);
				} catch (InterruptedException e) {
				}		
				for(int j=0; j<queues.length; ++j) 
					queues[j].constantDeque(j);	
				System.out.println("-------------------------------------------------------");
			}	System.out.println("Supplementary time...\n");
			while(areQueuesEmpty(queues) == false) {
				if(!isActive) return;
				try {
					Thread.sleep(1500);
				} catch (InterruptedException e) {
	
				}
				for(int j=0; j<queues.length; ++j) 
					queues[j].constantDeque(j);		
				System.out.println("-------------------------------------------------------");
			}   System.out.println("~~~~~ End of Simulation ~~~~~");
		}	
	});	
		return exit;
	}
	
	//methods used for extracting data after the simulation has ended:
	
	public String getAverageWaitingTime() {
		
		for(int i=0; i<queues.length; ++i)
			averageWaitingTime += queues[i].getWaitingTime();
	
		return String.format("Average waiting time: %.2f", (double)averageWaitingTime/Client.numberOfClients);
		
	}
	
	public String getAverageServiceTime() {
		
		return String.format("Average service time: %.2f", (double)totalServiceTime/Client.numberOfClients);
		
	}
	
	public String getEmptyQueueTime() {
		
		for(ClientQueue q: queues) 
			emptyQueueTime += q.getEmptyQueueCount();
		
		return "Total empty queues time: " + emptyQueueTime;
		
	}
	
	public String getTotalNumberOfClients() {
		
		return "Total number of clients: " + Client.numberOfClients;
		
	}
	
	public String getBiggestSizeQueue() {
		
		int max = queues[0].getBiggestSize();
		int index = 0;
		
		for(int i=0; i<queues.length; ++i)
			if(max < queues[i].getBiggestSize()) {
				max = queues[i].getBiggestSize();
				index = i;
			}
		
		return "Biggest queue in size: Queue No." + index + " with a number of " + max + " client(s)";
	}
	
	public String getPeakMinute() {
		
		return "Peak minute: " + peak;
		
	}
	
	/*
	 * changes the flag coordinating the simulation
	 * 
	 */
	public void stopSimulation() {
		
		isActive = false;
		
	}
	 
}
