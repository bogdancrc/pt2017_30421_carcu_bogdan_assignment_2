package queue_structure;

import java.util.ArrayList;
import java.util.List;

/*
 * Class that implements a queue of Client objects.
 * Its behavior is similar to a standard queue structure,
 * depicting methods for:
 * -> enqueue
 * -> dequeue
 * -> get its size
 */
public class ClientQueue {
	
	private List<Client> clients;
	private int totalWaitingTime;
	private int emptyQueueCount;     //is incremented every time the queue remains empty
	private int biggestSize;		 //holds the biggest size that the queue ever had
	
	public ClientQueue() {
		
		clients = new ArrayList<Client>();
	
	}
	
	public void enqueue(Client c) {
		
		clients.add(c);
		if(biggestSize < clients.size()) {
			biggestSize = clients.size();
		}
		
	}
	
	private void dequeue() {
		
		if(!clients.isEmpty())
			clients.remove(0);
		
	}
	
	public int getQueueSize() {
		
		return clients.size();
		
	}
	
	public int getWaitingTime() {
		
		return totalWaitingTime;
		
	}
	
	public int getEmptyQueueCount() {
		
		return emptyQueueCount;
		
	}
	
	public int getBiggestSize() {
		
		return biggestSize;
		
	}
	
	/*
	 * Decrements the first client's service time by one step
	 * and removing it from the line if its service time has come
	 * to an end. It is an enhanced dequeue method
	 */
	public void constantDeque(int index) {
		
			if(clients.size() == 0) {
				emptyQueueCount++;
				return;
			} else {
				
				totalWaitingTime += clients.size() - 1;
				System.out.println("Queue No." + index);
				System.out.println(this);

			}
			
			clients.get(0).decrementServiceTime();
			
			if(clients.get(0).getServiceTime() == 0) {
				dequeue();
			}
	}
	
	public String toString() {
		
		String result = "";
		
		for(Client c: clients) {
			
			result += c;
			result += "\n";
			
		}
		
		return result;
		
	}
	
}
