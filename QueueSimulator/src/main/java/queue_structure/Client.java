package queue_structure;

/*
 * Class that implements the expected behavior 
 * and attributes of a client waiting in line
 */
public class Client {

	public static int numberOfClients;    //for obtaining the total number of clients
	
	private String id;
	private int arrivalTime;
	private int serviceTime;
	
	public Client(int arrivalTime, int serviceTime) {
		
		id = "" + (++numberOfClients);
		setArrivalTime(arrivalTime);
		setServiceTime(serviceTime);

	}
	
	public void setArrivalTime(int arrivalTime) {
		
			this.arrivalTime = arrivalTime;
	
	}
	
	public int getArrivalTime() {
		
		return arrivalTime;
		
	}
	
	public void setServiceTime(int serviceTime) {
		
		this.serviceTime = serviceTime;
		
	}
	
	public int getServiceTime() {
		
		return serviceTime;
		
	}
	
	public void decrementServiceTime() {
		
		serviceTime--;
		
	}
	
	
	public String toString() {
		
		return "Client #" + id + " Arrival: " + arrivalTime + " " + "Service: " + serviceTime;
		
	}
	
}
