package PT2017.demo.QueueSimulator;

import gui.*;

/*
 * Main Application
 * 
 */
public class App {
	
    public static void main( String[] args ) {
    	
    	new Controller(new View(), new Model());
    	
    }
}
