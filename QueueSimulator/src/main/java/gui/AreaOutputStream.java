package gui;

import java.io.IOException;
import java.io.OutputStream;
import javax.swing.JTextArea;

/*
 * Class needed to redirect the output stream
 * to a selected JTextArea
 */
public class AreaOutputStream extends OutputStream {

	private JTextArea textArea;
	
	public AreaOutputStream(JTextArea textArea) {
		
			this.textArea = textArea;
			
	}
	
	public void write(int b) throws IOException {
		
		//redirects data to the text area
        textArea.append(String.valueOf((char)b));
        
        //scrolls the text area to the end of data
        textArea.setCaretPosition(textArea.getDocument().getLength());
	}
	
}
