package gui;

import queue_structure.Checkout;

/*
 * Class that provides the computational structure
 * behind the user interface.
 */
public class Model {

	private Checkout checkout;
	
	public void setCheckout(int simulation, int numberOfQueues, int minArrivalTime, int maxArrivalTime, 
							int minServiceTime, int maxServiceTime) throws Exception {
		
		checkout = new Checkout(simulation, numberOfQueues, minArrivalTime, maxArrivalTime, minServiceTime, maxServiceTime);
		
	}
	
	public Thread getEnterThread() {
		
		return checkout.enterQueues();
		
	}
	
	public Thread getExitThread() {
		
		return checkout.exitQueues();
		
	}
	
	public void stop() {
		
		checkout.stopSimulation();
		
	}
	
	public String getStatistics() {
		
		String result = "";
		result += checkout.getAverageServiceTime() + "\n";
		result += checkout.getAverageWaitingTime() + "\n" ;
		result += checkout.getBiggestSizeQueue() + "\n";
		result += checkout.getEmptyQueueTime() + "\n";
		result += checkout.getTotalNumberOfClients() + "\n";
		result += checkout.getPeakMinute() + "\n";
		
		return result;
	}
	
}
