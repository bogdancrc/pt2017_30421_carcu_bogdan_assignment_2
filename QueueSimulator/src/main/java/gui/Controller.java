package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import queue_structure.Client;

/*
 * Class that links View and Model classes 
 * Offers functionality to the user interface.
 */
public class Controller {
	
	private ExecutorService exec;
	
	public Controller(final View theView, final Model theModel) {
		
		//functionality of "Start" button
		theView.setStartListener(new ActionListener() {	
		
			public void actionPerformed(ActionEvent arg0) {
				
			try{	
				theModel.setCheckout(theView.getSimulationTime(), theView.getQueuesNumber(), theView.getMinArrivalTime(), 
									theView.getMaxArrivalTime(), theView.getMinServiceTime(), theView.getMaxServiceTime());
		
				theView.enableStatsButton(true);
				Thread t1 = theModel.getEnterThread();
				Thread t2 = theModel.getExitThread();
	
				exec = Executors.newFixedThreadPool(2);	
				exec.submit(t1);
				exec.submit(t2);
				exec.shutdown();
				
			
		} catch(Exception e) {
			
					theView.sendErrorMsg("An error ocurred! Simulation aborted.");
			}
			
		}	
			 
	});
		//functionality of "Refresh" button
		theView.setRefreshListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
			
				theView.refreshSimulationArea();
				theView.refreshStatisticsArea();
				Client.numberOfClients = 0;
			}
			
		});
		
		//functionality of "Stop" button
		theView.setStopListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
			try{
				theModel.stop();
				theView.enableStatsButton(false);
				Client.numberOfClients = 0;
				
				Thread.sleep(2000);
				
				System.out.println("Simulation was stopped due to user request.");
				} catch(Exception ex) {
					
					theView.sendErrorMsg("Application is already stopped!");
					
				}
			}
			
		});
		
		//functionality of "Show Stats" button
		theView.setShowListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				
				theView.setStatisticResult(theModel.getStatistics());
				theView.enableStatsButton(false);
				Client.numberOfClients = 0;
			}
			
		});
	}	
}
