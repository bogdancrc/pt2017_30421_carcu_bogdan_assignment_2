package gui;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.io.PrintStream;

import javax.swing.*;

/*
 * Class that builds the graphical user interface.
 */
@SuppressWarnings("serial")
public class View extends JFrame {
	
	private PrintStream printStream;
	private JPanel p1, p2, p3, p4, p5, p6, pFinal;
	private JLabel simulationTime, queuesNumber, minArrival, maxArrival, minService, maxService, statistics;
	private JTextField simulationTxt, queuesNoTxt, minArrivalTxt, maxArrivalTxt, minServiceTxt, maxServiceTxt;
	private JTextArea simulationArea, statisticArea;
	private JScrollPane scroll;
	private JButton start, refresh, stop, show;
	
	public View() {
		
		super("Bogdween Shopping Center");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(400, 650);
		setResizable(false);
		
		simulationTxt = new JTextField(3); queuesNoTxt = new JTextField(3); minArrivalTxt = new JTextField(3);
		maxArrivalTxt = new JTextField(3); minServiceTxt = new JTextField(3); maxServiceTxt = new JTextField(3);
		
		simulationTime = new JLabel("     Simulation time: ");
		queuesNumber = new JLabel("Number of queues: ");
		minArrival = new JLabel("Minimum arrival time:  ");
		maxArrival = new JLabel(" Maximum arrival time:    ");
		minService = new JLabel("Minimum service time: ");
		maxService = new JLabel("Maximum service time:   ");
		statistics = new JLabel("Statistics");
		
		start = new JButton("Start"); refresh = new JButton("Refresh"); 
		stop = new JButton("Stop"); show = new JButton("Show Stats");
		show.setEnabled(false);
	
		p1 = new JPanel(); p2 = new JPanel(); p3 = new JPanel(); p4 = new JPanel(); p5 = new JPanel(); p6 = new JPanel();
		pFinal = new JPanel();
		simulationArea = new JTextArea(20, 0); statisticArea = new JTextArea(1,1);
		simulationArea.setEditable(false); statisticArea.setEditable(false);
		scroll = new JScrollPane(simulationArea);
		
		printStream = new PrintStream(new AreaOutputStream(simulationArea));
		System.setOut(printStream);
		
		pFinal.setLayout(new BoxLayout(pFinal, BoxLayout.PAGE_AXIS));
		
		p1.add(simulationTime); p1.add(simulationTxt);
		p4.add(queuesNumber); p4.add(queuesNoTxt);
		pFinal.add(p1);
		pFinal.add(p4);
		
		p2.add(minArrival); p2.add(minArrivalTxt);
		p2.add(maxArrival); p2.add(maxArrivalTxt);
		pFinal.add(p2);
		
		p3.add(minService); p3.add(minServiceTxt);
		p3.add(maxService); p3.add(maxServiceTxt);
		pFinal.add(p3);
		
		p5.add(start); p5.add(refresh); p5.add(stop); p5.add(show);
		pFinal.add(p5);
		
		p6.add(statistics);	
		p6.setBackground(Color.LIGHT_GRAY);
		
		pFinal.add(scroll);
		pFinal.add(p6);
		pFinal.add(statisticArea);
		
		p5.setBackground(Color.LIGHT_GRAY);
		
		add(pFinal);
		setVisible(true);
	}
	
	public int getSimulationTime() {
		
		return Integer.parseInt(simulationTxt.getText());
		
	}
	
	public int getQueuesNumber() {
		
		return Integer.parseInt(queuesNoTxt.getText());
		
		
	}
	
	public int getMinArrivalTime() {
		
		return Integer.parseInt(minArrivalTxt.getText());
		
	}
	
	public int getMaxArrivalTime() {
		
		return Integer.parseInt(maxArrivalTxt.getText());
		
	}
	
	public int getMinServiceTime() {
		
		return Integer.parseInt(minServiceTxt.getText());
		
	}
	
	public int getMaxServiceTime() {
		
		return Integer.parseInt(maxServiceTxt.getText());
		
	}
	
	public void setStatisticResult(String results) {
		
			statisticArea.setText(results);
		
	}
	 
	public void sendErrorMsg(String msg){
	
		JOptionPane.showMessageDialog(null, msg, "Oops!", JOptionPane.ERROR_MESSAGE);
	
	}
	
	public void refreshSimulationArea() {
		
		simulationArea.setText("");
		repaint();
		
	}
	
	public void refreshStatisticsArea() {
		
		statisticArea.setText("");
		repaint();
	}
	
	public void enableStatsButton(boolean b) {
		
		show.setEnabled(b);
		
	}
	
	public void setStartListener(ActionListener listenForButton){
		
		start.addActionListener(listenForButton);
	}
	
	public void setRefreshListener(ActionListener listenForButton){
		
		refresh.addActionListener(listenForButton);
	}
	
	public void setStopListener(ActionListener listenForButton){
		
		stop.addActionListener(listenForButton);
	}
	
	public void setShowListener(ActionListener listenForButton){
		
		show.addActionListener(listenForButton);
	
	}
	
}
